package accion.com.roomdatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RoomDb")
data class Chapter(
    @PrimaryKey
    @ColumnInfo(name = "chapterName") val chapterName: String
) {
    companion object {
        fun populateData(): Array<Chapter> {
            return arrayOf<Chapter>(
                Chapter("One"),
                Chapter("Two"),
                Chapter("Three"),
                Chapter("Four"),
                Chapter("Five"),
                Chapter("Six"),
                Chapter("Seven"),
                Chapter("Eight"),
                Chapter("Nine"),
                Chapter("Ten"),
                Chapter("Eleven"),
                Chapter("Twelve")
            )
        }
    }
}