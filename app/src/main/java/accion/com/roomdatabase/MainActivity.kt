package accion.com.roomdatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private var chapterDatabase: ChapterDatabase? = null
    private var recyclerView: RecyclerView? = null

    private lateinit var searchView: SearchView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        chapterDatabase = ChapterDatabase.getDatabase(this)!!


        recyclerView = findViewById<RecyclerView>(R.id.lvSearchResult)
        recyclerView?.layoutManager = LinearLayoutManager(this)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_searchview, menu)
        val searchItem = menu!!.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        searchView.setSubmitButtonEnabled(true)
        searchView.setQueryHint("Search either - Numbers")
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                getNamesFromDb(newText)
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                getNamesFromDb(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun getNamesFromDb(searchText: String) {
        val searchTextQuery = "%$searchText%"
        chapterDatabase!!.chapterDao().getChapterName(searchTextQuery)
            .observe(this, object : Observer<List<Chapter>> {
                override fun onChanged(chapter: List<Chapter>?) {
                    if (chapter == null) {
                        return
                    }

                    var adapter = SearchAdapter(
                        this@MainActivity,
                        chapter
                    )
                    recyclerView?.adapter = adapter
                }
            })
    }
}